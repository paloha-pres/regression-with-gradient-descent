# Python implementation of univariate, multivariate and logistic regression using gradient descent algorithm and normal equation

This repository is strongly inspired by the Coursera course Machine Learning by Andrew Ng. Purpose of this repo is educational. 

## Running on Ubuntu

1. git clone https://gitlab.com/paloha-pres/regression-with-gradient-descent
1. cd regression-with-gradient-descent
1. sudo apt-get install python-virtualenv
1. virtualenv --python=python3 .venv
1. source .venv/bin/activate
1. pip install -r requirements.txt
1. jupyter notebook

Will also run on any other operating system. Adjust the commands according to your distro.
